# -*- coding: utf-8 -*-
# RAW (PCM) audio peak level meter
# Copyright (C) 2018  Sepp Jansen PA8Q
#
# This file is part of QStreamer
#
# QStreamer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# QStreamer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with QStreamer.  If not, see <http://www.gnu.org/licenses/>.
import threading
import time
import struct
import math

class VUMeter(object):
    """
    Simple PCM peak level meter
    """
    def __init__(self, freq, blocksize, callback, ref=0, rounding=1):
        """
        Args:
        freq: frequency in Hz to read the peak value and call the callback
        blocksize: framesize of the incoming audio
        callback: callback to call with the peak data
        ref: 0dB reference value
        rounding: precision of the float returned to the callback
        """
        self.peak = 0
        self.callback = callback
        self.rounding = rounding
        self.alive = True

        self.max = int(math.pow(2, 15))
        self.ref = math.pow(10.0, ref * -0.05)
        assert self.ref > 0.0
        self.time = 1/freq if freq < 50 else 1/50

        self.unpack = struct.Struct('{}h'.format(blocksize)).unpack

        self.vuProcThread = threading.Thread(target=self._proc)
        self.vuProcThread.daemon = True
        self.vuProcThread.name = 'VU processor'
        self.vuProcThread.start()

    def add_sound(self, pcm):
        """
        Add sound to get the peaks from
        Must be 16 bit integer and framesize must be the same as specified in initialisation
        Args:
        pcm: PCM audiodata
        """
        val = max(map(abs, self.unpack(pcm)))
        if val > self.peak:
            self.peak = val

    def stop(self):
        """
        Stops processing
        """
        self.alive = False

    def _proc(self):
        """
        Pulls the peak value, resets it, converts it to dB and calls the callback
        """
        while self.alive:
            time.sleep(self.time)

            val = self.peak
            self.peak = 0

            db = 20.0 * math.log10((val/self.max) * self.ref) if val > 0 else -math.inf

            self.callback(round(db, self.rounding))
