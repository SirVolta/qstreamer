# -*- coding: utf-8 -*-
# RAW (PCM) audio to ogg/opus icecast stream
# Copyright (C) 2018  Sepp Jansen PA8Q
#
# This file is part of QStreamer
#
# QStreamer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# QStreamer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with QStreamer.  If not, see <http://www.gnu.org/licenses/>.

import shouty
import queue
from pyogg.opus import *
import ctypes
import threading
import time
import logging

PYOPUSICE_ERR_ENCODEFAIL = 1
PYOPUSICE_ERR_ICEFAIL = 2
PYOPUSICE_ERR_ENCODER = 3
PYOPUSICE_ERR_STOP = -2

# Samplerate is always 48000
SAMPLERATE = 48000

appname = "QStreamer: pyopusice"

log = logging.getLogger(appname)
log.setLevel(logging.DEBUG)
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
formatter = logging.Formatter('[%(name)s] [%(levelname)s] [%(asctime)s]: %(message)s')
handler.setFormatter(formatter)
log.addHandler(handler)


class IcecastOut(threading.Thread):
    def __init__(self, bitrate, framesize, ice_params, complexity=None, music=True):
        """
        Args:
        bitrate: Stream bitrate in kbit/s
        framesize: the amount of samples delivered to the add_sound function each call
        ice_params: icecast streaming parameters
        complexity: the complexity used by the opuscoder. Omit for opus default
        music: True: optimize for music, False: optimize for voice
        """
        threading.Thread.__init__(self)
        # Holds frames of ogg-encapsulated opus data
        self.opusdata = queue.Queue(maxsize=6000)
        # Holds one framesize of raw pcm bytes
        self.pcm = queue.Queue(maxsize=6000)
        # Icecast parameters
        self.ice_params = ice_params
        self.bitrate = bitrate
        self.framesize = framesize
        # Icecast connection
        # self.ice = shouty.connection.Connection(**params_general)
        # self.ice.open()
        self.error = 0
        self.complexity = complexity
        self.music = music

        self.daemon = True
        self.start()

    def __del__(self):
        # self.ice.close()
        # self.ice.free()
        self.error = PYOPUSICE_ERR_STOP

    def _set_opus_param(self, param, arg=None):
        """
        Sets a opus encoder parameter
        """
        if arg:
            error = ope_encoder_ctl(self.encoder, param, arg)
        else:
            error = ope_encoder_ctl(self.encoder, param)
        if error:
            log.error("Failed to set param {} to {} eror: {}".format(param, arg, error))

        return error

    def _opus_write_calback(self, params, opus_data, length):
        """
        Called by libopusenc with one frame of opus data
        """
        # Convert the c_ubyte_p to a python bytes object
        encd = ctypes.string_at(opus_data, length)
        # And place it into the queue
        self.opusdata.put(encd)
        return OPUS_OK

    def _opus_close_callback(self, params):
        """
        Called by libopusenc if it closes
        """
        log.info("Opus encoder closed {}".format(params))
        self.error = PYOPUSICE_ERR_ENCODER

    def run(self):
        """
        Thread handling encoding of PCM data to opus
        """
        # Format is always OGG
        self.ice_params['format'] = shouty.Format.OGG

        # Create the track comments
        com = ope_comments_create()

        # Prepare the callbacks
        callbacks = OpusEncCallbacks()
        callbacks.write = ope_write_func(self._opus_write_calback)
        callbacks.close = ope_close_func(self._opus_close_callback)

        # Create the encoder
        err = ctypes.c_int(-12345)
        errp = pointer(err)
        self.encoder = ope_encoder_create_callbacks(callbacks, None, com, 48000, 1, 0, errp)
        if err:
            log.error("Failed to create the opus coder {}".format(err))
            self.error = PYOPUSICE_ERR_ENCODER
            return

        # Configure the codec
        self._set_opus_param(OPUS_SET_BITRATE_REQUEST, self.bitrate * 1000)
        self._set_opus_param(OPUS_SET_VBR_REQUEST, 1)
        if self.music:
            log.info("{}: set for music".format(self.ice_params['mount']))
            self._set_opus_param(OPUS_SET_SIGNAL_REQUEST, OPUS_SIGNAL_MUSIC)
            self._set_opus_param(OPUS_SET_APPLICATION_REQUEST, OPUS_APPLICATION_AUDIO)
        else:
            log.info("{}: set for voice".format(self.ice_params['mount']))
            self._set_opus_param(OPUS_SET_APPLICATION_REQUEST, OPUS_APPLICATION_VOIP)
            self._set_opus_param(OPUS_SET_SIGNAL_REQUEST, OPUS_SIGNAL_VOICE)
        if self.complexity:
            self._set_opus_param(OPUS_SET_COMPLEXITY_REQUEST, self.complexity)

        # ope_encoder_flush_header(self.encoder)

        # Start the icecast worker
        self.senderThread = threading.Thread(target=self._sender)
        self.senderThread.daemon = True
        self.senderThread.name = 'IcecastSender'
        self.senderThread.start()

        log.info("Initialized icecast {}".format(self.ice_params['mount']))

        # dot-optimizing aliases for CPU
        get = self.pcm.get
        cast = ctypes.cast
        encoder = self.encoder
        framesize = self.framesize
        while not self.error:
            pcm = get()
            pcm_int = cast(pcm, opus_int16_p)
            error = ope_encoder_write(encoder, pcm_int, framesize)
            if error:
                log.error("Coding error: {}".format(error))
                self.error = PYOPUSICE_ERR_ENCODER

    def set_comments(self, comments):
        """
        Start a new track with comments:
        https://www.xiph.org/vorbis/doc/v-comment.html
        Args:
        comments: dictionary containing the new track comments
        """
        com = ope_comments_create()
        for key, value in comments.items():
            ope_comments_add(com, key.encode('UTF-8'), value.encode('UTF-8'))
        ope_encoder_chain_current(self.encoder, com)

    def stop(self):
        # Setting error will stop both threads and make them do their cleanup
        self.error = PYOPUSICE_ERR_STOP

    def _sender(self):
        """
        Background thread that handles the transmission of ogg-opus data to icecast
        """
        # Do not send header right away so the socket will not timeout
        # if the user application does not start sending data within a few seconds
        # of starting the icecast sender
        while self.opusdata.qsize() <3:
            time.sleep(0.1)

        with shouty.connect(**self.ice_params) as ice:
            while not self.error:
                data = self.opusdata.get()
                err = ice.send(data)
                if err:
                    log.error("Icecast error: {}".format(err))
                    self.error = PYOPUSICE_ERR_ICEFAIL
                ice.sync()

    def add_sound(self, pcm):
        """
        Add sound to the stream.
        Must be 16 bit PCM and must have the framesize specified when initializing
        """
        self.pcm.put(pcm)
